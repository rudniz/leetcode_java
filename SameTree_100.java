import java.util.*;

public class SameTree_100 {
    public static void main(String[] args){
        Solution s = new Solution();
        List<Integer> arr = new ArrayList<>(List.of(1,1,2));
        TreeNode head = arrayToTree(arr);
        List<Integer> arr2 = new ArrayList<>(List.of(1,1,2));
        TreeNode head2 = arrayToTree(arr2);
        System.out.println(s.isSameTreeQueue(head, head2));

    }

    private static TreeNode arrayToTree(List<Integer> arr){
        if(arr.size() == 0) return null;
        TreeNode head = new TreeNode(arr.get(0));
        for(int i: arr.subList(1, arr.size())){
            insert(head, i);
            }
        return head;
    }

    private static void insert(TreeNode node, int data){
        if(node == null) return;
        if(data < node.val){
            if(node.left == null){
                node.left = new TreeNode(data);
            }
            else insert(node.left, data);
        }
        else {
            if(node.right == null){
                node.right = new TreeNode(data);
            }
            else insert(node.right, data);
        }
    }

    private static void printTree(TreeNode node){
        if(node == null) return;
        System.out.println(String.valueOf(node.val));
        printTree(node.left);
        printTree(node.right);
    }


    static class Solution {
        public boolean isSameTree(TreeNode p, TreeNode q) {
            if(p == null && q == null) return true;
            if(p == null || q == null) return false;
            if (p.val == q.val) {
                return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
            }
            return false;
        }

        public boolean isSameTreeStack(TreeNode p, TreeNode q) {
            if(p == null && q == null) return true;
            if(p == null || q == null) return false;
            Stack<TreeNode> stackQ = new Stack<>();
            Stack<TreeNode> stackP = new Stack<>();
            stackP.push(p);
            stackQ.push(q);
            while (!stackQ.isEmpty() && !stackP.isEmpty()){
                TreeNode tempP = stackP.pop();
                TreeNode tempQ = stackQ.pop();
                if(tempP.val != tempQ.val) return false;
                if (tempP.left != null && tempQ.left != null) {
                    stackP.push(tempP.left);
                    stackQ.push(tempQ.left);
                }
                else if (tempP.left == null && tempQ.left == null) {
                }
                else {
                    return false;
                }

                if(tempP.right != null && tempQ.right != null){
                    stackP.push(tempP.right);
                    stackQ.push(tempQ.right);
                }
                else if (tempP.right == null && tempQ.right == null){
                } else return false;
            }
            return stackP.size() == stackQ.size();
        }

        public boolean isSameTreeQueue(TreeNode p, TreeNode q){
            Queue<TreeNode> queue = new LinkedList<>();
            queue.add(p);
            queue.add(q);
            while (!queue.isEmpty()){
                TreeNode a = queue.poll();
                TreeNode b = queue.poll();
                if(a == null && b == null) continue;
                if(a == null || b == null || a.val != b.val) return false;
                queue.add(a.left);
                queue.add(b.left);
                queue.add(a.right);
                queue.add(b.right);
            }
            return true;
        }

        public boolean isSameTree2(TreeNode p, TreeNode q) {
            List<Integer> a = traverse(p);
            List<Integer> b = traverse(q);
            return a.equals(b);
        }

        private List<Integer> traverse(TreeNode node){
            List<Integer> res = new ArrayList<>();
            if(node == null) res.add(0);
            else{
                res.add(node.val);
                res.addAll(traverse(node.left));
                res.addAll(traverse(node.right));
            }
            return res;
        }
    }
}
