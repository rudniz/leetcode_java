public class SearchInsertPosition_35 {
    public static void main(String[] args){
        Solution s = new Solution();
        int[] nums = {1,3,5,6};
//        System.out.println(String.valueOf(s.searchInsert2(nums, 5)));
//        System.out.println(String.valueOf(s.searchInsert2(nums, 7)));
        System.out.println(String.valueOf(s.searchInsert2(nums, 7)));



    }
    static class Solution {
        public int searchInsert(int[] nums, int target) {
            if(nums.length == 0) return 0;
            for(int i = 0; i < nums.length; i ++){
                if(target == nums[i] || target < nums[i]) return i;

            }
            return nums.length;
        }

        public int searchInsert2(int[] nums, int target){
            int l = nums.length - 1;
//            if (l == 0) return 0;
//            if (target < nums[0]) return 0;
//            if(target > nums[l]) return l + 1;
            int start = 0;
            int end = l;
            while (start <= end){
                int mid = (start + end)/2;
                if(nums[mid] == target) return mid;
                if(nums[mid] > target) end = mid - 1;
                else start = mid + 1;
            }

            return start;

        }
    }
}
