public class MaximumSubbaray_53 {
    public static void main(String[] args){
        Solution solution = new Solution();
        int[] arr = {-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(solution.maxSubArray(arr));
    }

    static class Solution{
        public int maxSubArray(int[] nums) {
            int curentMaximum = nums[0];
            int currentSum = nums[0];
            for(int i = 1; i < nums.length; i++){
                if(currentSum < 0){
                    currentSum = 0;
                }
                currentSum += nums[i];

                curentMaximum = Math.max(curentMaximum, currentSum);
            }
            return curentMaximum;

        }
    }
}
