public class PalindromeNumber_09 {

    public static void main(String[] args){
        PalindromeNumber_09 p = new PalindromeNumber_09();
        System.out.println(p.isPalindrome2(121));
        System.out.println(p.isPalindrome2(-121));
        System.out.println(p.isPalindrome2(500));
        System.out.println(p.isPalindrome2(2147447412));
    }
    private boolean isPalindrome(int x){
        if(x < 0){
            return false;
        }
        int temp = x;
        int res = 0;
        while (temp > 0){
            res = res * 10 + temp % 10;
            temp = temp / 10;
        }
        return res == x;
    }

    private boolean isPalindrome2(int x){
        if(x < 0){
            return false;
        }
        int left = 0;
        int right = 0;
        int range = 1;
        while(x / range >= 10){
            range *= 10;
        }
        while (x > 0){
            left = x / range;
            right = x % 10;
            if(left!= right){
                return false;
            }
            x = (x % range)/ 10;
            range /= 100;
        }
        return true;
    }

}

