public class RemoveElement_27 {

    public static void main(String[] args){
        Solution s = new Solution();
        int[] arr = {0,1,2,2,3,0,4,2};
        System.out.println(s.removeElement(arr, 2));
        for(int i: arr){
            System.out.println(String.valueOf(i));
        }


    }

    static class Solution {
        public int removeElement(int[] nums, int val) {
           int l = nums.length;
           int m = 0;
           for(int i = 0; i < l; i++){
               if(nums[i] != val){
                   nums[m] = nums[i];
                   m++;
               }
           }
        return m;
        }
    }

}
