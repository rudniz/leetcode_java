public class MergeLinkedLists_21 {
    public static void main(String[] args){
        ListNode l = new ListNode(1);
        ListNode l2 = new ListNode(2);
        ListNode l3 = new ListNode(4);
        l.next = l2;
        l2.next = l3;
        ListNode k = new ListNode(1);
        ListNode k1 = new ListNode(3);
        ListNode k2 = new ListNode(4);
        k.next = k2;
        Solution s = new Solution();
        ListNode res = s.mergeTwoListsRecursion(l, k);
        printll(res);
        System.out.println();

    }

    private static void printll(ListNode res){
        ListNode t = res;
        while (t != null){
            System.out.println(t.val);
            t = t.next;
        }
    }
    static class Solution {
        public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
            if(l1 == null){
                return l2;
            }
            if(l2 == null){
                return l1;
            }
            ListNode head = new ListNode(0);
            ListNode prev = head;

            while (l1 != null && l2 != null){
                if (l1.val < l2.val){
                    prev.next = l1;
                    l1 = l1.next;
                }
                else {
                    prev.next = l2;
                    l2 = l2.next;
                }
                prev = prev.next;
            }
            if(l1 != null){
                prev.next = l1;
            }
            else if(l2 != null){
                prev.next = l2;
            }
            return head.next;
        }

        public ListNode mergeTwoListsRecursion(ListNode l1, ListNode l2) {
            if(l1 == null){
                return l2;
            }
            if(l2 == null){
                return l1;
            }
            if(l1.val < l2.val){
                l1.next = mergeTwoListsRecursion(l1.next, l2);
                return l1;
            }
            else {
                l2.next = mergeTwoListsRecursion(l1, l2.next);
                return l2;
            }
        }
    }
}
