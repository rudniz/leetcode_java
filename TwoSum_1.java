import java.util.HashMap;

public class TwoSum_1 {
    public static void main(String [ ] args) {
        int[] nums = {3,2,4};
        int t = 6;
        Solution s = new Solution();
        int[] res = s.twoSum(nums, t);
        for(int i: res){
            System.out.println(String.valueOf(i));
        }
    }
}

class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        int l = nums.length;
        int key;
        for(int i = 0; i < l; i++){
            key = target - nums[i];
           if(hashMap.containsKey(key)){
               return new int[]{hashMap.get(key), i};
           }
           else{
               hashMap.put(nums[i], i);
           }
        }
        return new int[2];
    }
}