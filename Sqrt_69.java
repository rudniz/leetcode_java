public class Sqrt_69 {
    public static void main(String[] args){
        Solution solution = new Solution();
        System.out.println(solution.mySqrt3(0));
        System.out.println(solution.mySqrt3(1));
        System.out.println(solution.mySqrt3(2147395599));



    }
    static class Solution {
        public int mySqrt(int x) {
            long end = x;
            long start = 0;
            long mid = 0;
            while(true){
                mid = (start + end)/2;
                if((mid * mid) <= x && ((mid + 1) * (mid + 1)) > x){
                    return (int) mid;
                }
                if((mid * mid) > x) end = mid;
                else if(mid * mid < x) start = mid;
            }
        }

        public int mySqrt2(int x) {
            long r = x;
            while (r * r > x){
                r = (r + x/r)/2;
            }
            return (int) r;
        }

        public int mySqrt3(int x){
            int end = x;
            int start = 0;
            while (start < end){
                int mid = (start + end + 1)/2;
                if(mid > x/mid) end = mid - 1;
                else start = mid;
            }
            return end;
        }
    }
}
