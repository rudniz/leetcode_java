public class LongestCommomPrefix_14 {

    public static void main(String[] args){
        String[][] strs = {{"flower","flow","flight"}, {}, {"dog","racecar","car"}, {"dogg", "dog", "do"}};
        Solution s = new Solution();
        for(String[] string: strs){
            System.out.println(s.longestCommonPrefix2(string));
        }
    }

    private static class Solution {
        public String longestCommonPrefix(String[] strs) {
            if(strs == null || strs.length == 0)    return "";
            String pre = strs[0];
            int i = 1;
            while(i < strs.length){
                while(strs[i].indexOf(pre) != 0)
                    pre = pre.substring(0,pre.length()-1);
                i++;
            }
            return pre;
        }

        public String longestCommonPrefix2(String[] strs) {
            if(strs == null || strs.length <1) return "";
            String first = strs[0];
            String last = strs[0];
            for(String s: strs){
                if(s.compareTo(first) < 0){
                    first = s;
                }
                if(s.compareTo(last) > 0){
                    last = s;
                }
            }
            int l = Math.min(first.length(), last.length());
            int i = 0;
            while (i < l){
                if(first.charAt(i) != last.charAt(i)){
                    break;
                }
                else i++;
            }
            return first.substring(0, i);
        }

        public String longestCommonPrefix3(String[] strs) {
            if(strs.length < 1){
                return "";
            }
            int l1,l2, l;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(strs[0]);
            for(int j = 1; j < strs.length; j++){
                l1 = strs[j].length();
                l2 = stringBuilder.length();
                l = (l1 < l2) ? l1 : l2;
                stringBuilder.delete(l, l2);
                l2 = l;
                for(int i = 0; i < l; i++)
                    if(stringBuilder.charAt(i) != strs[j].charAt(i)){
                        stringBuilder.delete(i, l2);
                        break;
                    }
            }
            return stringBuilder.toString();
        }
    }
}
