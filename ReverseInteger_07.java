public class ReverseInteger_07 {
    static class Solution {
        public int reverse(int x) {
            int res = 0;
            int tempResult = 0;
            boolean isNegative = false;
            if(x < 0){
                isNegative = true;
                x = x * -1;
            }
            while (x > 0){
                tempResult = res * 10;
                tempResult += x % 10;
                if((tempResult - x%10)/ 10 != res){
                    return 0;
                }
                x = x/10;
                res = tempResult;
            }
            if(isNegative){
                res = res * -1;
            }
            return res;
        }

        public int reverse2(int x) {
            int res = 0;
            boolean isNegative = false;
            if(x < 0){
                isNegative = true;
                x *= -1;
            }
            while (x > 0){
                try {
                    res = Math.multiplyExact(res, 10);
                    res = Math.addExact(res, x % 10);
                    x = x / 10;
                } catch(ArithmeticException e) {
                    return 0;
                }
            }
            if(isNegative){
                res = res * -1;
            }
            return res;
        }
    }

    public static void main(String [ ] args) {
        Solution s = new Solution();
        System.out.println(String.valueOf(s.reverse2(
                1534236469)));
        System.out.println(String.valueOf(s.reverse2(
                -123)));
    }
}
