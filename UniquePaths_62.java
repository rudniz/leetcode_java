public class UniquePaths_62 {
    public static void main(String[] args){
        Solution s = new Solution();
        System.out.println(String.valueOf(s.uniquePaths(7, 3)));

    }
    static class Solution {
        public int uniquePaths(int m, int n) {
            if(m == 1 || n == 1) return 1;
            m--;
            n--;
            int s = m + n;
            if(m < n){
                int t = m;
                m = n;
                n = t;
            }
            long res = 1;
            int j = 1;
            for(int i = m + 1; i <= s; i++){
                res *= i;
                res /= j;
                j++;
            }
            return (int)res;

        }
    }
}
