public class StrStr_28 {
    public static void main(String[] args){
        Solution s = new Solution();
        System.out.println(s.strStr("bbaaaaac", "ca"));

    }
    static class Solution {
        public int strStr(String haystack, String needle) {
            if(needle.equals("")) return 0;
            for(int i = 0; ; i++){
                for(int j = 0; ; j++){
                    if(j == needle.length()) return i;
                    if((i + j) == haystack.length()) return -1;
                    if(haystack.charAt(i + j) != needle.charAt(j)) break;
                }
            }
        }
    }
}
