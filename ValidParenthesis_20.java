import java.util.Stack;

public class ValidParenthesis_20 {

    public static void main(String[] args){
        Solution solution = new Solution();
        System.out.println(solution.isValid2("()[]{}"));
        System.out.println(solution.isValid2("([{}])"));
        System.out.println(solution.isValid2("([)]"));
        System.out.println(solution.isValid2("]"));
        System.out.println(solution.isValid2("["));
    }

    static class Solution {
        public boolean isValid(String s) {
            Stack<String> stack = new Stack<>();
            for(char c: s.toCharArray()){
                switch (c){
                    case '[':
                        stack.push("[");
                        break;
                    case '(':
                        stack.push("(");
                        break;
                    case '{':
                        stack.push("{");
                        break;
                    case '}':
                        if(stack.empty() || !stack.pop().equals("{")) return false;
                        break;
                    case ']':
                        if(stack.empty() || !stack.pop().equals("[")) return false;
                        break;
                    case ')':
                        if(stack.empty() || !stack.pop().equals("(")) return false;
                        break;
                    }
                }
            if(!stack.empty()) return false;
            return true;
        }

        public boolean isValid2(String s) {
            Stack<Character> stack = new Stack<>();
            for(char c: s.toCharArray()){
                switch (c){
                    case '(':
                        stack.push(')');
                        break;
                    case '{':
                        stack.push('}');
                        break;
                    case '[':
                        stack.push(']');
                        break;
                    case ')' : case '}' : case ']':
                        if(stack.empty() || stack.pop() != c) return false;
                }
            }
            return stack.empty();
        }
    }
}
