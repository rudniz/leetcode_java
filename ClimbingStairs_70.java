import java.util.HashMap;

public class ClimbingStairs_70 {
    public static void main(String[] args){
       Solution s = new Solution();
       System.out.println(s.climbStairs2(10));
    }

    static class Solution {
        HashMap<Integer, Integer> dict = new HashMap<>();

        public int climbStairs(int n) {
            if(n == 1) return 1;
            if(n == 2) return 2;
            int first = 1;
            int second = 2;
            int cur = 0;
            for(int i = 3; i <= n; i++){
                cur = first + second;
                first = second;
                second = cur;
            }
            return cur;

        }

        public int climbStairs2(int n) {
            if(n == 1) return 1;
            if(n == 2) return 2;
            if(dict.containsKey(n)) return dict.get(n);
            else{
                int t = climbStairs2(n - 1) + climbStairs2(n - 2);
                dict.put(n, t);
                return t;
            }

        }
    }
}
