public class RemoveDuplicatesArray_26 {

    public static void main(String[] args){
        int[] arr = {0,0,1,1,1,2,2,3,3,4};
        Solution s = new Solution();
        System.out.println(String.valueOf(s.removeDuplicates(arr)));
        for(int i: arr){
            System.out.println(String.valueOf(i));
        }

    }
    static class Solution {
        public int removeDuplicates(int[] nums) {
            int i = 0;
            for(int j = 1; j < nums.length; j++){
                if(nums[i] != nums[j]){
                    i += 1;
                    nums[i] = nums[j];
                }
            }
            return i + 1;
        }
    }
}
