public class PlusOne_66 {

    public static void main(String[] args){
        Solution solution = new Solution();
        int[] arr = {9,9,9};
        arr = solution.plusOne(arr);
        for(int i: arr){
            System.out.println(String.valueOf(i));
        }

    }

    static class Solution {
        public int[] plusOne2(int[] digits) {
            long temp = 0;
            for(int i: digits){
                temp *= 10;
                temp += i;
            }
            temp += 1;
            int n = String.valueOf(temp).length();
            int[] res = new int[n];
            for(int i = n - 1; i >= 0; i--){
                res[i] = (int)(temp % 10);
                temp = temp / 10;
            }
            return res;
        }

        public int[] plusOne(int[] digits) {
            int l = digits.length - 1;
            for(int i = l; i >= 0; i--){
                if(digits[i] != 9){
                    digits[i] += 1;
                    return digits;
                }
                else{
                    digits[i] = 0;
                }
            }
            digits = new int[l + 2];
            digits[0] = 1;
            return digits;
        }
    }
}
