public class CountAndSay_38 {
    public static void main(String[] args){
        Solution solution = new Solution();
        System.out.println(solution.countAndSay(3));
    }

    static class Solution {
        public String countAndSay(int n) {
            StringBuilder s = new StringBuilder("1");

            for (int i = 1; i < n; i++){
                StringBuilder temp = new StringBuilder();
                int count = 1;
                char prev = s.charAt(0);
                for(int j = 1; j < s.length(); j ++){
                    if(s.charAt(j) != prev){
                        temp.append(count).append(prev);
                        prev = s.charAt(j);
                        count = 1;
                    }
                    else count+= 1;
                }
                temp.append(count).append(prev);
                s = temp;
            }
            return s.toString();
        }
    }
}
