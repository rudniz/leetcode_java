public class LenghtOfLastWord {
    public static void main(String[] args){
        Solution solution = new Solution();
        System.out.println(solution.lengthOfLastWord(" llk   "));

    }

    static class Solution {
        public int lengthOfLastWord(String s) {
            if(s.length() == 0) return 0;
            int count = 0;
            int length = s.length() - 1;
            while (length >=0 && s.charAt(length) == ' '){
                length--;
            }
            while (length >=0 && s.charAt(length) != ' '){
                length--;
                count++;
            }
            return count;

        }
    }
}
