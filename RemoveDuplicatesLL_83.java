public class RemoveDuplicatesLL_83 {
    public static void main(String[] args){
        Solution s = new Solution();
        ListNode a = new ListNode(1);
        ListNode b = new ListNode(1);
        ListNode c = new ListNode(2);
        ListNode d = new ListNode(2);
        ListNode e = new ListNode(2);
        a.next = b;
        b.next = c;
        c.next = d;
        d.next = e;
        printLinkedList(s.deleteDuplicates(a));
    }

    private static void printLinkedList(ListNode head){
        while (head != null){
            System.out.println(String.valueOf(head.val));
            head = head.next;
        }
    }

    static class Solution {
        public ListNode deleteDuplicates(ListNode head) {
            ListNode temp = head;
            while (temp != null){
                while (temp.next!= null && temp.val == temp.next.val){
                    temp.next = temp.next.next;
                }
                temp = temp.next;
                }
            return head;
            }
        }
}
